import 'package:flutter/material.dart';
import 'package:login/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<LoginPage> {
  bool _showPass = false;
  bool _showEmailErrorMessage = true;
  bool _showPasswordErrorMessage = true;
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passController = new TextEditingController();
  dynamic _emailError = null;
  dynamic _passError = null;
  var _emailInvalid = false;
  var _passInvalid = false;
  bool _validInput = false;
  var _correctEmail = null;
  var _correctPassword = null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.fromLTRB(30, 10, 30, 20),
        constraints: const BoxConstraints.expand(),
        color: Colors.white,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              /*---------------LOGO--------------*/
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: Container(
                  width: 70,
                  height: 70,
                  padding: const EdgeInsets.all(15),
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: Color(0xffd8d8d8)),
                  child: const FlutterLogo(),
                ),
              ),
              /*---------------Title--------------*/
              const Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
                child: Text(
                  'Hello\nWelcome Back',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 25,
                  ),
                ),
              ),
              /*---------------EMAIL INPUT--------------*/
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: TextField(
                  onChanged: (val) {
                    validInput();
                    _correctEmail = true;
                  },
                  controller: _emailController,
                  style: const TextStyle(fontSize: 18, color: Colors.black),
                  decoration: const InputDecoration(
                      labelText: 'EMAIL',
                      labelStyle:
                          TextStyle(color: Color(0xff888888), fontSize: 15)),
                ),
              ),
              _correctEmail == false && _showEmailErrorMessage
                  ? Text(
                      _emailError,
                      style: TextStyle(color: Colors.red),
                    )
                  : SizedBox(),
              /*---------------PASSWORD INPUT--------------*/
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                child: Stack(
                  alignment: AlignmentDirectional.centerEnd,
                  children: <Widget>[
                    TextField(
                      controller: _passController,
                      obscureText: !_showPass,
                      style: const TextStyle(fontSize: 18, color: Colors.black),
                      onChanged: (val) {
                        validInput();
                        _correctPassword = true;
                      },
                      decoration: const InputDecoration(
                          labelText: 'PASSWORD',
                          labelStyle: TextStyle(
                              color: Color(0xff888888), fontSize: 15)),
                    ),
                    /*---------------SHOW PASSWORD BUTTON--------------*/
                    GestureDetector(
                      onTap: onToggleShowPass,
                      child: Text(_showPass ? 'HIDE' : 'SHOW',
                          style: const TextStyle(
                              color: Colors.blue,
                              fontSize: 13,
                              fontWeight: FontWeight.bold)),
                    ),
                  ],
                ),
              ),
              _correctPassword == false
                  ? Text(
                      _passError,
                      style: TextStyle(color: Colors.red),
                    )
                  : SizedBox(),
              /*---------------SIGN IN BUTTON--------------*/
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                child: SizedBox(
                  width: double.infinity,
                  height: 40,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                    ),
                    onPressed: _validInput ? onSignInClicked : null,
                    child: const Text(
                      'SIGN IN',
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onSignInClicked() {
    checkEmail();
    checkPassword();

    if (_emailInvalid == false &&
        _passInvalid == false &&
        _correctPassword &&
        _correctEmail) {
      Navigator.push(context, MaterialPageRoute(builder: gotoHomePage));
    }
  }

  Widget gotoHomePage(BuildContext context) {
    return HomePage();
  }

  void onToggleShowPass() {
    setState(() {
      _showPass = !_showPass;
    });
  }

  bool checkLengthEmail() {
    return _emailController.text.length >= 6;
  }

  bool checkLengthPassword() {
    return _passController.text.length >= 6;
  }

  bool checkContentEmail() {
    return _emailController.text.contains('@') &&
        _emailController.text.contains('.');
  }

  bool checkCorrectEmail() {
    return _emailController.text == 'dungnt@dehasoft.com';
  }

  bool checkPasswordCorrect() {
    return _passController.text == '123456';
  }

  void checkEmail() {
    if (checkLengthEmail() == false || checkContentEmail() == false) {
      setState(() {
        _emailInvalid = true;
      });
    } else if (checkCorrectEmail() == false) {
      setState(() {
        _correctEmail = false;
        _emailError = 'Email không tồn tại';
      });
    } else {
      _emailError = null;
      _emailInvalid = false;
      _correctEmail = true;
    }
  }

  void checkPassword() {
    if (checkLengthPassword() == false) {
      setState(() {
        _passInvalid = true;
      });
    } else if (checkPasswordCorrect() == false) {
      setState(() {
        _correctPassword = false;
        _passError = 'Password không đúng';
      });
    } else {
      _passInvalid = false;
      _passError = null;
      _correctPassword = true;
    }
  }

  void validInput() {
      setState(() {
        _validInput = checkLengthPassword() && checkLengthEmail() && checkContentEmail();
      });
  }
}
